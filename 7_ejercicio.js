//Leer un número entero de dos digitos y determinar si los dos digitos son iguales
let numero=29;
if(numero>=10 && numero<=99){
    const digito=parseInt(numero/10);
    const digito2=parseInt(numero%10);
    if(digito==digito2){
        console.log(`${digito} son iguales ${digito2}`);
    }
    else{
        console.log(`${digito} no son iguales ${digito2}`);
    }
}
else{
    console.log(numero,' debe ser un numero de dos digitos');
}