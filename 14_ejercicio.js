//Leer un número entero y mostrar en pantalla su tabla de multiplicar

//Funcion que genera las tablas de multiplicar del 1 al 10
function TablasMultiplicar(numero) {

    console.log(`\nTabla del ${numero}`);
    for (let i = 1; i < 11; i++) {
        console.log(`${numero} X ${i} = ${numero*i}`);
    }

}

let numero=11;
TablasMultiplicar(numero);