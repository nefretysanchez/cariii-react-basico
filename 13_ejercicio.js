//Generar todas las tablas de multiplicar del 1 al 10

//Funcion que genera las tablas de multiplicar del 1 al 10
function TablasMultiplicar() {
    for (let tabla = 1; tabla < 11; tabla++) {
        console.log(`\nTabla del ${tabla}`);
        for (let numero = 1; numero < 11; numero++) {
            console.log(`${tabla} X ${numero} = ${tabla * numero}`);
        }
    }
}

TablasMultiplicar();