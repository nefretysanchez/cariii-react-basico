//Leer dos numeres enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números.
//Funcion para determinar si es divisor exacto
function divisor(num, num2){
    if(num%num2==0){
        return true;
    }
    else{
        return false;
    }
}

let numero=60;
let numero2=50;
let resta=numero-numero2;
let divisor_numero=divisor(numero,resta);
let divisor_numero2=divisor(numero2,resta);
if(divisor_numero==true && divisor_numero2==true){
    console.log(`${resta} es divisor exacto de ${numero} y ${numero2}`);
}
else if(divisor_numero==true){
    console.log(`${resta} es divisor de ${numero}`);
}
else if(divisor_numero2==true){
    console.log(`${resta} es divisor de ${numero2}`);
}
else{
    console.log(`${resta} no es divisor de ${numero} y ${numero2}`);
}