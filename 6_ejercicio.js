//Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.

let numero=99;
if(numero>=10 && numero<=99){
    const digito=parseInt(numero/10);
    const digito2=parseInt(numero%10);
    if(digito%digito2==0){
        console.log(`${digito} es multiplo de ${digito2}`);
    }
    else if(digito2%digito==0){
        console.log(`${digito2} es multiplo de ${digito}`);
    }
    else{
        console.log(`${digito2} y ${digito} no son multiplos del otro`);
    }
}
else{
    console.log(numero,' debe ser un numero de dos digitos');
}