//Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 
//Funcion para determinar numeros iguales
function iguales(digito,digito2){
    if(digito==digito2){
        console.log(`${digito} son iguales ${digito2}`);
    }
    else{
        console.log(`${digito} no son iguales ${digito2}`);
    }
}

let numero=1968;
if(numero>=1000 && numero<=9999){
    const digito=parseInt(numero/1000);
    const digito2=parseInt((numero%1000)/100);
    const digito3=parseInt((numero%100)/10);
    const digito4=parseInt((numero%100)%10);
    iguales(digito2,digito3);
}
else{
    console.log(numero,' debe ser un numero de cuatro digitos');
}