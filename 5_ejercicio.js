//Leer un número entero de dos digitos y determinar si es primo y además si es negativo.

//Funcion para determinar si el numero es primo
function primo(numero) {
    let primo=[];
    for (let i = 2; i <= numero - 1; i++) {
        primo[i] = numero % i;
    }
    if (primo.indexOf(0) == -1) {
        console.log(`${numero} es un numero primo`);
    }
    else {
        console.log(`${numero} no es un numero primo`);
    }
}

//Funcion para determinar negativos
function negativo(num) {
    if (num < 0) {
        console.log(num, ' es un numero negativo');
    }
    else {
        console.log(num, ' es un numero positivo');
    }
}

let numero = 28;
if (numero >= 10 && numero <= 99) {
    const digito = parseInt(numero / 10);
    const digito2 = parseInt(numero % 10);
    primo(digito);
    negativo(digito);
    primo(digito2);
    negativo(digito2);
}
else {
    console.log(numero, ' debe ser un numero de dos digitos')
}

