//Leer un número entero y determinar cuantas veces tiene el digito 1

let numero = 6110;
let suma = 0;
let comparacion=1;
numero = numero.toString();

for (let i = 0; i < numero.length; i++) {
    if(numero[i]==comparacion){
        suma++;
    }
}

console.log(`La cantidad de ${comparacion} en ${numero} es ${suma}`);