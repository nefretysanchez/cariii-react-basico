//Leer tres numeros enteros y determinar si el último digito. de los tres numeros es igual. 
//Funcion para extraer ultimo digito
function ultimo_digito(num){
    digito=num%10;
    return digito;
}

//Funcion para saber si son numeros de dos digitos
function dos_digitos(num){
    if(numero>=10 && numero<=99){
        return true;
    }
    else{
        return false;
    }
}

let numero=96;
let numero2=85;
let numero3=65;
let validacion_numero=dos_digitos(numero);
let validacion_numero2=dos_digitos(numero2);
let validacion_numero3=dos_digitos(numero3);
if(validacion_numero==true && validacion_numero2==true && validacion_numero3==true){
    let digito=ultimo_digito(numero);
    let digito2=ultimo_digito(numero2);
    let digito3=ultimo_digito(numero3);
    if(digito==digito2 && digito2==digito3){
        console.log(`${digito}, ${digito2} y ${digito3} son iguales`);
    }
    else{
        console.log(`${digito}, ${digito2} y ${digito3} no son iguales`);
    }
}
else{
    console.log('Deben ser numeros de dos digitos')
}