//Leer un número entena de dos digitos y determinar si ambos digitos son pares

//Funcion para determinar numeros pares
function pares(num){
    if(num%2==0){
        return true;
    }
    else{
        return false;
    }
}


let numero=23;
if(numero>=10 && numero<=99){
    const digito=parseInt(numero/10);
    const digito2=parseInt(numero%10);
    const digitopar=pares(digito);
    const digito2par=pares(digito2);
    if(digitopar==true && digito2par==true){
        console.log(digito, ' y ', digito2, ' son numeros pares')
    }
    else if(digitopar==false && digito2par==true){
        console.log(digito, ' es impar y ', digito2, ' es par')
    }
    else if(digito2par==false && digitopar==true){
        console.log(digito, ' es par y ', digito2, ' es impar')
    }
    else{
        console.log(digito, ' y ', digito2, ' son numeros impares')
    }
}
else{
    console.log(numero,' debe ser un numero de dos digitos')
}