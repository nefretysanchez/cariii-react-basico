//Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos
let numero=20;
if(numero>=10 && numero<=99){
    const digito=parseInt(numero/10);
    const digito2=parseInt(numero%10);
    const suma=digito+digito2;
    console.log('La suma de los digitos de ', numero,' es ', suma);
}
else{
    console.log(numero,' debe ser un numero de dos digitos')
}