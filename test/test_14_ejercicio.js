//Leer un número entero y mostrar en pantalla su tabla de multiplicar

//Funcion que genera las tablas de multiplicar del 1 al 10
function TablasMultiplicar(numero) {
    let response;
    response=`\nTabla del ${numero}`;
    for (let i = 1; i < 11; i++) {
        response=`${numero} X ${i} = ${numero*i}`;
    }
    return `Se genero la tabla del ${numero}`;
}

module.exports=TablasMultiplicar;