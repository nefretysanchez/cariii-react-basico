const numberIsPrime_Negative=require('./test_5_ejercicio');

test('1 es un numero primo 1 es un numero positivo 4 no es un numero primo 4 es un numero positivo', () => {
    expect(numberIsPrime_Negative(14)).toBe("1 es un numero primo 1 es un numero positivo 4 no es un numero primo 4 es un numero positivo");
})

test('Debe ser un numero de dos digitos', () => {
    expect(numberIsPrime_Negative(2)).toBe("debe ser un numero de dos digitos");
});