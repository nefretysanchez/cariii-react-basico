const numberMultiple=require('./test_6_ejercicio')

test('6 es multiplo de 2', () => {
    expect(numberMultiple(26)).toBe("6 es multiplo de 2");
});

test('9 es multiplo de 3', () => {
    expect(numberMultiple(93)).toBe("9 es multiplo de 3");
});

test('No son multiplos del otro', () => {
    expect(numberMultiple(38)).toBe("8 y 3 no son multiplos del otro");
});

test('Debe ser un numero de dos digitos', () => {
    expect(numberMultiple(2)).toBe("debe ser un numero de dos digitos");
});