const countDigit=require('./test_12_ejercicio');

test('La cantidad de 1 en 36 es 0', () => {
    expect(countDigit(36)).toBe("La cantidad de 1 en 36 es 0");
});

test('La cantidad de 1 en 1401 es 2', () => {
    expect(countDigit(1401)).toBe("La cantidad de 1 en 1401 es 2");
});