const posicionArray=require('./test_16_ejercicio')

test('Los numeros terminados en 0 estan en la posicion: 2,4', () => {
    expect(posicionArray(3,56,50,1,70,98,14,36,25,36)).toBe("Los numeros terminados en 0 estan en la posicion: 2,4");
});