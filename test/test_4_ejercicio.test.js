const numberIsPrime=require('./test_4_ejercicio');

test('14 no es un numero primo', () => {
    expect(numberIsPrime(14)).toBe("14 no es un numero primo");
})

test('13 es un numero primo', () => {
    expect(numberIsPrime(13)).toBe("13 es un numero primo");
})

test('El numero debe estar entre 10 y 20', () => {
    expect(numberIsPrime(26)).toBe("debe ser un numero entre 10 y 20");
})