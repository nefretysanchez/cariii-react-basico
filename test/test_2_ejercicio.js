function sumDigit(number){
    let response;
    if(number>=10 && number<=99){
        const digito=parseInt(number/10);
        const digito2=parseInt(number%10);
        const sum=digito+digito2;
        response= sum;
        return response;
    }
    else{
        return "debe ser un numero de dos digitos";
    }
}

module.exports=sumDigit;