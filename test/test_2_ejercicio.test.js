const sumDigit=require('./test_2_ejercicio');

test('La suma de 25 es 7', () => {
    expect(sumDigit(25)).toBe(7);
});

test('Debe ser un numero de dos digitos', () => {
    expect(sumDigit(2)).toBe("debe ser un numero de dos digitos");
});