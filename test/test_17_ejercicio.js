function repeatArray(numero, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9) {
    let response;
    let arrayNumber = [numero, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9];
    let mayor=Math.max(...arrayNumber);

    let posiciones=arrayNumber.filter((item) => item==mayor);
    
    response = `El numero mayor ${mayor} esta repetido ${posiciones.length} veces `;
    return response;
}

module.exports=repeatArray;