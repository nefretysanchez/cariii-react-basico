function numberIsPrime(number) {
    let primo = [];
    let response;
    if (number >= 10 && number <= 20) {
        for (let i = 2; i <= number - 1; i++) {
            primo[i] = number % i;
        }
        if (primo.indexOf(0) == -1) {
            response=`${number} es un numero primo`;
        }
        else {
            response=`${number} no es un numero primo`;
        }
        return response;
    }
    else {
        return "debe ser un numero entre 10 y 20";
    }
}

module.exports=numberIsPrime;