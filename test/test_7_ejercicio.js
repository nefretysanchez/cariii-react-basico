function isEqualDigit(number) {
    let response;
    if (number >= 10 && number <= 99) {
        const digito = parseInt(number / 10);
        const digito2 = parseInt(number % 10);
        if (digito == digito2) {
            response=`${digito} son iguales ${digito2}`;
        }
        else {
            response=`${digito} no es igual a ${digito2}`;
        }
    }
    else {
        response='debe ser un numero de dos digitos';
    }
    return response;
}
module.exports=isEqualDigit;