//Leer tres numbers enteros y determinar si el último digito. de los tres numbers es igual. 
//Funcion para extraer ultimo digito
function ultimo_digito(num) {
    digito = num % 10;
    return digito;
}

//Funcion para saber si son numbers de dos digitos
function dos_digitos(number) {
    if (number >= 10 && number <= 99) {
        return true;
    }
    else {
        return false;
    }
}

function isEqualDigits(number, number2, number3) {
    let response;
    let validacion_number = dos_digitos(number);
    let validacion_number2 = dos_digitos(number2);
    let validacion_number3 = dos_digitos(number3);
    if (validacion_number == true && validacion_number2 == true && validacion_number3 == true) {
        let digito = ultimo_digito(number);
        let digito2 = ultimo_digito(number2);
        let digito3 = ultimo_digito(number3);
        if (digito == digito2 && digito2 == digito3) {
            response=`${digito}, ${digito2} y ${digito3} son iguales`;
        }
        else {
            response=`${digito}, ${digito2} y ${digito3} no son iguales`;
        }
        return response;
    }
    else {
        return 'Deben ser numero de dos digitos';
    }

}

module.exports=isEqualDigits;