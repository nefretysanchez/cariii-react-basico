//Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 
//Funcion para determinar numeros iguales
function iguales(digito, digito2) {
    let response;
    if (digito == digito2) {
        response=`${digito} es igual a ${digito2}`;
    }
    else {
        response=`${digito} no es igual a ${digito2}`;
    }
    return response;
}

function digitIsEqual(number) {
    let response;
    if (number >= 1000 && number <= 9999) {
        const digito = parseInt(number / 1000);
        const digito2 = parseInt((number % 1000) / 100);
        const digito3 = parseInt((number % 100) / 10);
        const digito4 = parseInt((number % 100) % 10);
        response=iguales(digito2, digito3);
        return response;
    }
    else {
        return 'debe ser un numero de cuatro digitos';
    }
}

module.exports=digitIsEqual;