const digitIsEqual= require('./test_8_ejercicio');

test('6 no es igual a 9', () => {
    expect(digitIsEqual(2698)).toBe("6 no es igual a 9");
});

test('8 es igual a 8', () => {
    expect(digitIsEqual(2889)).toBe("8 es igual a 8");
});

test('Debe ser un numero de cuatro digitos', () => {
    expect(digitIsEqual(22)).toBe("debe ser un numero de cuatro digitos");
});