const numberNegative = require('./test_1_ejercicio.js');

test('-350 es negativo', () => {
    expect(numberNegative(-5)).toBe("es un numero negativo");
});

test('350 es positivo', () => {
    expect(numberNegative(58)).toBe("es un numero positivo");
});
