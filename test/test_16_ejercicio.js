function posicionArray(numero, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9) {
    let response;
    let arrayNumber = [numero, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9];
    let pos = [];

    for (let i = 0; i < arrayNumber.length; i++) {
        if (arrayNumber[i] % 10 == 0) {
            pos.push(i);
        }
    }
    response = `Los numeros terminados en 0 estan en la posicion: ${pos}`;
    return response;
}

module.exports=posicionArray;