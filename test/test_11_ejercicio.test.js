const sumDigits=require('./test_11_ejercicio')

test('La suma de 36 es 9', () => {
    expect(sumDigits(36)).toBe("La suma de los digitos de 36 es 9");
});

test('La suma de 6 es 6', () => {
    expect(sumDigits(6)).toBe("La suma de los digitos de 6 es 6");
});