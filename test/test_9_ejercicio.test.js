const isEqualDigits=require('./test_9_ejercicio')


test('Los digitos no son iguales', () => {
    expect(isEqualDigits(22,22,22)).toBe("2, 2 y 2 son iguales");
});

test('Los digitos son iguales', () => {
    expect(isEqualDigits(22,36,94)).toBe("2, 6 y 4 no son iguales");
});

test('Debe ser un numero de dos digitos', () => {
    expect(isEqualDigits(22)).toBe("Deben ser numero de dos digitos");
});
