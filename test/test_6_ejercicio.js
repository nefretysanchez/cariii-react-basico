function numberMultiple(number){
    let response;
    if(number>=10 && number<=99){
        const digito=parseInt(number/10);
        const digito2=parseInt(number%10);
        if(digito%digito2==0){
            response=`${digito} es multiplo de ${digito2}`;
        }
        else if(digito2%digito==0){
            response=`${digito2} es multiplo de ${digito}`;
        }
        else{
            response=`${digito2} y ${digito} no son multiplos del otro`;
        }
    }
    else{
        response='debe ser un numero de dos digitos';
    }
    return response;
}

module.exports=numberMultiple;