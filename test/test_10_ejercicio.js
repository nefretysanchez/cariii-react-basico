//Leer dos numeres enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números.
//Funcion para determinar si es divisor exacto
function divisor(num, num2) {
    if (num % num2 == 0) {
        return true;
    }
    else {
        return false;
    }
}

function isDivisor(number, number2) {
    let response;
    let resta = number - number2;
    let divisor_number = divisor(number, resta);
    let divisor_number2 = divisor(number2, resta);
    if (divisor_number == true && divisor_number2 == true) {
        response=`${resta} es divisor exacto de ${number} y ${number2}`;
    }
    else if (divisor_number == true) {
        response=`${resta} es divisor de ${number}`;
    }
    else if (divisor_number2 == true) {
        response=`${resta} es divisor de ${number2}`;
    }
    else {
        response=`${resta} no es divisor de ${number} y ${number2}`;
    }
    return response;
}

module.exports=isDivisor;