const isDivisor=require('./test_10_ejercicio');

test('No son divisores', () => {
    expect(isDivisor(22,36)).toBe("-14 no es divisor de 22 y 36");
});

test('Son divisores exactos', () => {
    expect(isDivisor(60,50)).toBe("10 es divisor exacto de 60 y 50");
});
