function MaxArray(numero, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9) {
    let response;
    let arrayNumber = [numero, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9];

    let mayor = Math.max(...arrayNumber);
    response = `El mayor de ${arrayNumber} es ${mayor}`;
    return response;
}

module.exports=MaxArray;