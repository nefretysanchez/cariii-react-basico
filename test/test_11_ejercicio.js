//Leer un número entero y determinar a cuánto es igual la suma de sus digitos.

function sumDigits(number) {
    let response;
    let suma = 0;

    number = number.toString();

    for (let i = 0; i < number.length; i++) {
        suma = suma + parseInt(number[i])
    }

    response = `La suma de los digitos de ${number} es ${suma}`;
    return response;

}

module.exports=sumDigits;