const isEvenDigit= require('./test_3_ejercicio');

test('Los dos numeros son pares', () => {
    expect(isEvenDigit(26)).toBe("Los dos numeros son pares");
})

test('2 es par y 5 es impar', () => {
    expect(isEvenDigit(25)).toBe("2 es par y 5 es impar");
})

test('5 es impar y 6 es par', () => {
    expect(isEvenDigit(56)).toBe("5 es impar y 6 es par");
})

test('Los dos numeros son impares', () => {
    expect(isEvenDigit(35)).toBe("Los dos numeros son impares");
})

test('Debe ser un numero de dos digitos', () => {
    expect(isEvenDigit(2)).toBe("debe ser un numero de dos digitos");
});