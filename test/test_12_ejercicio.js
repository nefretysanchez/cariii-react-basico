//Leer un número entero y determinar cuantas veces tiene el digito 1

function countDigit(number) {
    let response;
    let suma = 0;
    let comparacion = 1;
    number = number.toString();

    for (let i = 0; i < number.length; i++) {
        if (number[i] == comparacion) {
            suma++;
        }
    }

    response=`La cantidad de ${comparacion} en ${number} es ${suma}`;
    return response;
}

module.exports=countDigit;