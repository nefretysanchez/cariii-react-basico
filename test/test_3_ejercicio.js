function pares(num) {
    if (num % 2 == 0) {
        return true;
    }
    else {
        return false;
    }
}

function isEvenDigit(number) {
    let response;
    if (number >= 10 && number <= 99) {
        const digito = parseInt(number / 10);
        const digito2 = parseInt(number % 10);
        const digitopar = pares(digito);
        const digito2par = pares(digito2);
        if (digitopar == true && digito2par == true) {
            return "Los dos numeros son pares";
        }
        else if (digitopar == false && digito2par == true) {
            response = `${digito} es impar y ${digito2} es par`;
            return response;

        }
        else if (digito2par == false && digitopar == true) {
            response = `${digito} es par y ${digito2} es impar`;
            return response;
        }
        else {
            return "Los dos numeros son impares";
        }
    }
    else {
        return "debe ser un numero de dos digitos";
    }
}

module.exports = isEvenDigit;