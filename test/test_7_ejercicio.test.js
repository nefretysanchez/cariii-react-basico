const isEqualDigit=require('./test_7_ejercicio');

test('Los digitos son iguales', () => {
    expect(isEqualDigit(22)).toBe("2 son iguales 2");
});

test('5 no es igual a 8', () => {
    expect(isEqualDigit(58)).toBe("5 no es igual a 8");
});

test('Debe ser un numero de dos digitos', () => {
    expect(isEqualDigit(2)).toBe("debe ser un numero de dos digitos");
});