function primo(number) {
    let response;
    let primo = [];
    for (let i = 2; i <= number - 1; i++) {
        primo[i] = number % i;
    }
    if (primo.indexOf(0) == -1) {
        response = `${number} es un numero primo`;
    }
    else {
        response = `${number} no es un numero primo`;
    }
    return response;
}

function negativo(num) {
    let response;
    if (num < 0) {
        response = `${num} es un numero negativo`;
    }
    else {
        response = `${num} es un numero positivo`;
    }
    return response;
}

function numberIsPrime_Negative(number) {
    let response,response1,response2,response3;
    let message;
    if (number >= 10 && number <= 99) {
        const digito = parseInt(number / 10);
        const digito2 = parseInt(number % 10);
        response=primo(digito);
        response1=negativo(digito);
        response2=primo(digito2);
        response3=negativo(digito2);
        message=response+' '+response1+' '+response2+' '+response3;
        return message;
    }
    else {
        return "debe ser un numero de dos digitos";
    }

}

module.exports=numberIsPrime_Negative;
