//Generar todas las tablas de multiplicar del 1 al 10

//Funcion que genera las tablas de multiplicar del 1 al 10
function TablasMultiplicar() {
    let response=[];
    for (let tabla = 1; tabla < 11; tabla++) {
        response=`\nTabla del ${tabla}`;
        for (let numero = 1; numero < 11; numero++) {
            response=`${tabla} X ${numero} = ${tabla * numero}`;
        }
    }
    return "Se generaron todas las tablas de multiplicar del 1-10";

}

module.exports=TablasMultiplicar;
